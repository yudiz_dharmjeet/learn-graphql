import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Character from './pages/Character';
import CharactersList from './pages/CharactersList';
import Search from './pages/Search';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/'>
            <Route index element={<CharactersList />} />
            <Route path='search' element={<Search />} />
            <Route path=':id' element={<Character />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
