import React, { useState } from 'react'
import { gql, useLazyQuery } from '@apollo/client'

const GET_CHARACTER_LOCATION = gql`
  query GetCharacterLocation($name: String!) {
    characters(filter: { name: $name }) {
      results {
        location {
          id
          name
        }
      }
    }
  }
`

function Search() {

  const [name, setName] = useState("")

  const [getLocations, {error, loading, data, called}] = useLazyQuery(GET_CHARACTER_LOCATION, {
    variables: {
      name
    }
  })

  console.log({
    called,
    loading,
    error,
    data
  })

  function handleChange(e) {
    setName(e.target.value)
  }

  return (
    <div>
      <input value={name} onChange={(e) => handleChange(e)} />
      <button onClick={() => getLocations()}>Search</button>

      {loading && <div>Spinner...</div>}

      {error && <div>Something went wrong...</div>}

      {data && (
        <ul>
          {data.characters.results.map((character) => {
            return <li key={character.location.id}>{character.location.name}</li>
          })}
        </ul>
      )}
    </div>
  )
}

export default Search