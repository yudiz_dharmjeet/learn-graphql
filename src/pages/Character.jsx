import React from 'react'
import { useParams } from 'react-router-dom'
import './Character.css'
import { useCharacter } from '../hooks/useCharacter'

function Character() {

  const { id } = useParams()

  const {error, loading, data} = useCharacter(id)

  if (error) {
    return <div>Something went wrong...</div>
  }

  if (loading) {
    return <div>Loading...</div>
  }

  return (
    <div className='Character'>
      <img src={data.character.image} alt={data.character.name} width={600} height={600} />
      <div className="Character-content">
        <h1>{data.character.name}</h1>
        <p>{data.character.gender}</p>
        <div className='Character-episode'>
          {data.character.episode.map(episode => {
            return <div key={episode.episode}>
              {episode.name} - <b>{episode.episode}</b>
            </div>
          })}
        </div>
      </div>
    </div>
  )
}

export default Character